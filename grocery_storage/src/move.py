#!/usr/bin/python
import sys
import rospy
import actionlib
import math
from geometry_msgs.msg import Pose, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from play_motion_msgs.msg import PlayMotionAction, PlayMotionGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from actionlib_msgs.msg import *

class MovementHandler:
    def __init__(self):
        self.turn_speed = 0.5
        self.move_pub = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size = 10)
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        self.move_base.wait_for_server(rospy.Duration(5))
        self.torso_cmd = rospy.Publisher('/torso_controller/command', JointTrajectory, queue_size=1)
        self.head_cmd = rospy.Publisher('/head_controller/command', JointTrajectory, queue_size=1)
        self.pm_as = actionlib.SimpleActionClient('/play_motion', PlayMotionAction)
        if not self.pm_as.wait_for_server(rospy.Duration(20)):
        	rospy.logerr("Could not connect to /play_motion AS")
        	exit()

    def prepare_robot(self):
        mg = PlayMotionGoal()
        mg.motion_name = 'pick_final_pose'
        mg.skip_planning = False
        self.pm_as.send_goal_and_wait(mg)

    def set_turn_speed(self, speed):
        self.turn_speed = speed

    def move_to_pose(self, pose):
        goal = MoveBaseGoal()
        goal.target_pose = pose
        self.move_base.send_goal(goal)
        success = self.move_base.wait_for_result(rospy.Duration(60))
        state = self.move_base.get_state()
        result = False
        if success and state == GoalStatus.SUCCEEDED:
            result = True
        else:
            result = False
        self.goal_sent = False
        return result

    def turn(self, angle, rad=False):
        #if angle is in degrees convert to radians
        if not rad:
            angle = angle * math.pi/180

        turn_speed = max(0.9 * (abs(angle)/(2*math.pi)), 0.5)

        #set twist for turn message
        turn = Twist()
        left = True
        if angle >= 0:
            turn.angular.z = turn_speed
        else:
            left = False
            turn.angular.z = -turn_speed

        #rotate
    	t0 = rospy.Time.now().to_sec()
    	curr_angle = 0
        if not left:
            angle = angle * -1
        while(curr_angle < angle):
            self.move_pub.publish(turn)
            t1 = rospy.Time.now().to_sec()
            curr_angle = turn_speed * (t1-t0)

    def lift_torso(self):
        jt = JointTrajectory()
        jt.joint_names = ['torso_lift_joint']
        jtp = JointTrajectoryPoint()
        jtp.positions = [0.34]
        jtp.time_from_start = rospy.Duration(2.5)
        jt.points.append(jtp)
        self.torso_cmd.publish(jt)
        rospy.sleep(2.5) #wait for robot to assume pose

    def lower_head(self):
        jt = JointTrajectory()
        jt.joint_names = ['head_1_joint', 'head_2_joint']
        jtp = JointTrajectoryPoint()
        jtp.positions = [0.0, -0.75]
        jtp.time_from_start = rospy.Duration(2.0)
        jt.points.append(jtp)
        self.head_cmd.publish(jt)
        rospy.sleep(2.5) #wait for robot to assume pose

    def scan_head(self, angle):
        jt = JointTrajectory()
        jt.joint_names = ['head_1_joint', 'head_2_joint']
        jtp = JointTrajectoryPoint()
        jtp.positions = [angle, -0.75]
        jtp.time_from_start = rospy.Duration(2.0)
        jt.points.append(jtp)
        self.head_cmd.publish(jt)
        rospy.sleep(2.5) #wait for robot to assume pose


    def raise_head(self):
        rospy.loginfo("Moving head up")
        jt = JointTrajectory()
        jt.joint_names = ['head_1_joint', 'head_2_joint']
        jtp = JointTrajectoryPoint()
        jtp.positions = [0.0, 0.0]
        jtp.time_from_start = rospy.Duration(2.0)
        jt.points.append(jtp)
        self.head_cmd.publish(jt)
        rospy.sleep(4) #wait for robot to assume pose
        rospy.loginfo("Done.")
