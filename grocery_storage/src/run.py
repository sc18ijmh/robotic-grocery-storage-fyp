#!/usr/bin/python
import sys
import rospy
import message_filters
import cv2
import actionlib
import itertools
import tf2_ros
import tf
import math
import numpy as np
import copy
from move import MovementHandler
from geometry_msgs.msg import Pose, PoseArray, PointStamped, TransformStamped, Point, PoseStamped
from cv_bridge import CvBridge, CvBridgeError
from shape_msgs.msg import SolidPrimitive
from moveit_msgs.msg import CollisionObject
from actionlib_msgs.msg import *
from std_msgs.msg import Bool
from sensor_msgs.msg import PointCloud2, Image

# Action server messages
from execute_grasp_moveit_grasps.msg import ExecGraspAction, ExecGraspGoal, ExecPlaceAction, ExecPlaceGoal
from visualization_msgs.msg import Marker

# Server messages
from simple_grasp_estimation.srv import GenerateGrasps, GeneratePlaces
from lasr_object_detection_yolo.srv import *
from jeff_segment_objects.srv import SegmentObjects
from plane_detection.srv import DetectPlanes, OnSurface, MergePcs
from setup_collision_scene.srv import SetupCollision

# Get moveit error codes
from moveit_msgs.msg import MoveItErrorCodes
moveit_error_dict = {}
for name in MoveItErrorCodes.__dict__.keys():
	if not name[:1] == '_':
		code = MoveItErrorCodes.__dict__[name]
		moveit_error_dict[code] = name

class GroceriesHandler:
	def __init__(self):
		self.grasping = False   	#grasp being carried out
		self.current_object = "apple"	#class of held object
		self.ignore_class = rospy.get_param("grocery_storage/ignore_class")
		self.movement_handler = MovementHandler()
		self.tf_buffer = tf2_ros.Buffer()
		self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

		#setup clients
		rospy.wait_for_service('/yolo_detection')
		rospy.wait_for_service('/segment_objects')
		rospy.wait_for_service('/detect_planes')
		rospy.wait_for_service('/setup_collision_scene')
		rospy.wait_for_service('/simple_grasps')
		rospy.wait_for_service('/simple_placings')
		try:
			self.yolo = rospy.ServiceProxy('/yolo_detection', YoloDetection)
			self.segment_objects = rospy.ServiceProxy('/segment_objects', SegmentObjects)
			self.detect_planes = rospy.ServiceProxy('/detect_planes', DetectPlanes)
			self.on_surface = rospy.ServiceProxy('/point_on_plane', OnSurface)
			self.setup_scene = rospy.ServiceProxy('/setup_collision_scene', SetupCollision)
			self.sg = rospy.ServiceProxy('/simple_grasps', GenerateGrasps)
			self.sp = rospy.ServiceProxy('/simple_placings', GeneratePlaces)
			self.place_as = actionlib.SimpleActionClient('/place_server', ExecPlaceAction)
			self.place_as.wait_for_server()
			self.pick_as = actionlib.SimpleActionClient('/pickup_server', ExecGraspAction)
			rospy.sleep(1.0)
			if not self.pick_as.wait_for_server(rospy.Duration(20)):
				rospy.logerr("Could not connect to /pickup server AS")
				exit()
			self.bridge = CvBridge()

			#visulaisation publishers
			self.detection_pub = rospy.Publisher('/grocery_storage/object_detection_image', Image, queue_size = 100)
			self.marker_pub = rospy.Publisher('/grocery_storage/placing_markers', Marker, queue_size = 1000)
		except rospy.ServiceException as e:
			print("Service call failed: %s"%e)
		
		rospy.loginfo("Setup all service proxies and action clients.")

		
		#MOVE TO TABLE#
		#get table poses for picking and placing
		temp = PoseStamped()
		temp.header.frame_id = "map"
		temp.header.stamp = rospy.Time.now()
		temp.pose.position.x = rospy.get_param("/grocery_storage/tables/pick_table/pose/position/x")
		temp.pose.position.y = rospy.get_param("/grocery_storage/tables/pick_table/pose/position/y")
		temp.pose.position.z = rospy.get_param("/grocery_storage/tables/pick_table/pose/position/z")
		temp.pose.orientation.x = rospy.get_param("/grocery_storage/tables/pick_table/pose/orientation/x")
		temp.pose.orientation.y = rospy.get_param("/grocery_storage/tables/pick_table/pose/orientation/y")
		temp.pose.orientation.z = rospy.get_param("/grocery_storage/tables/pick_table/pose/orientation/z")
		temp.pose.orientation.w = rospy.get_param("/grocery_storage/tables/pick_table/pose/orientation/w")
		self.pick_table = temp

		temp = PoseStamped()
		temp.header.frame_id = "map"
		temp.header.stamp = rospy.Time.now()
		temp.pose.position.x = rospy.get_param("/grocery_storage/tables/place_table/pose/position/x")
		temp.pose.position.y = rospy.get_param("/grocery_storage/tables/place_table/pose/position/y")
		temp.pose.position.z = rospy.get_param("/grocery_storage/tables/place_table/pose/position/z")
		temp.pose.orientation.x = rospy.get_param("/grocery_storage/tables/place_table/pose/orientation/x")
		temp.pose.orientation.y = rospy.get_param("/grocery_storage/tables/place_table/pose/orientation/y")
		temp.pose.orientation.z = rospy.get_param("/grocery_storage/tables/place_table/pose/orientation/z")
		temp.pose.orientation.w = rospy.get_param("/grocery_storage/tables/place_table/pose/orientation/w")
		self.place_table = temp

		self.groups = rospy.get_param("/grocery_storage/groups")

		rospy.loginfo("Moving to pick table")
		self.movement_handler.move_to_pose(self.pick_table)
		self.movement_handler.lift_torso()
		self.movement_handler.lower_head()

		#get planes
		self.update_planes()
		

		#make synced clients for pointcloud and rgb image
		self.pc_sub = message_filters.Subscriber('/xtion/depth_registered/points', PointCloud2)
		self.img_sub = message_filters.Subscriber('/xtion/rgb/image_rect_color', Image)
		self.detection_subs = [self.pc_sub, self.img_sub]
		self.sync = message_filters.TimeSynchronizer(self.detection_subs, 1)
		self.sync.registerCallback(self.detect_objects)


	def get_group(self):
		""" 
			Returns the list of items in the same group as the currently
			held item.

			Returns:
				string[]: list of class_ids for objects in the same group
		"""
		for group, names in self.groups.items():
			for name in names:
				if self.current_object == name:
					return names
		else:
			return []


	def euclidean_dist(self, point1, point2):
		"""
			Returns the euclidean distance between two points

			Arguments:
				geometry_msgs/Point: point 1 
				geometry_msgs/Point: point 2

			Returns:
				float: euclidean distance between points
		"""
		return math.sqrt(pow(point1.x - point2.x, 2) +
						pow(point1.y - point2.y, 2) +
						pow(point1.z - point2.z, 2))


	def sort_points(self, points, table_plane, matched_objects):
		"""
			Scores and sorts a set of points (desc.) based on their distance from the center of
			the storage surface, the distance to objects in the same group as the currently
			held item, and the distance to objects not in the same group.

			Arguments:
				geometry_msgs/Point[]: list of points to score and sort
				plane_detection/Plane: plane by which to sort points (prefer points closer to center)
				ExtendedCluster[]: list of objects present on the plane (prefer points closer to 
				similar items/further from disimilar items)
			
			Returns:
				geometry_msgs/Point[]: list of points sorted in descending order by score
		"""
		max_score = 0	#for visualisation
		min_score = 0

		table_position = copy.deepcopy(table_plane.pose.position)
		table_position.x -= 0.22 # place objects a bit closer to robot for better object detection

		group = self.get_group()

		sort_points = []
		for point in points:
			score = 0
			score -= self.euclidean_dist(point, table_position) * 0.2 #prioritise points nearer center of table
			for object in matched_objects:
				if object.class_id in group:
					score -= self.euclidean_dist(point, object.position)
				else:
					score += (self.euclidean_dist(point, object.position) + 
							self.euclidean_dist(point, table_position) * 0.2)


			sort_points.append(SortPoint(point, score))
			
			if max_score < score:
				max_score = score
			elif min_score > score:
				min_score = score
		
		#shift scores so lowest = 0
		if min_score < 0:
			max_score -= min_score
			for point in sort_points:
				point.score -= min_score
			min_score = 0
		
		sort_points = sorted(sort_points, key = lambda p: p.score, reverse=True)
		points = [point.point for point in sort_points]

		#visualise placing locations where high scores are green and low red
		scale_max = max_score - min_score
		for idx, point in enumerate(sort_points):
			red = ((scale_max-point.score)/scale_max) * 1
			green = (point.score/scale_max) * 1
			self.vis_point(point.point, idx, [red, green, 0], point.score)

		return points


	def gen_place_points(self, plane):
		"""
			Generate a set of 3D points a set distance above a given Plane such that
			each point is above the oriented bounding box for that plane.

			Arguments:
				Plane: plane above which to generate points 
			
			Returns:
				geometry_msgs/Point[]: oriented grid of points above given plane (in same frame as plane)
		"""

		#obtain transformation matrix from plane pose
		trans = tf.TransformerROS()
		translation = (plane.pose.position.x, plane.pose.position.y, plane.pose.position.z)
		rotation = (plane.pose.orientation.x, plane.pose.orientation.y, plane.pose.orientation.z, plane.pose.orientation.w)
		R = trans.fromTranslationRotation(translation, rotation)
		points = []

		#generate grid of points around origin (within bounds of plane dimensions)
		ty, tz = plane.size.y/2, plane.size.z/2
		by, bz = -(plane.size.y/2), -(plane.size.z/2)
		y, z = by, bz
		while(y < ty):
			while(z < tz):
				point = Point()
				point.x = 0
				point.y = y
				point.z = z

				#transform each point into planes frame of reference
				point_vector = np.zeros(4)
				point_vector[0] = point.x
				point_vector[1] = point.y
				point_vector[2] = point.z
				point_vector[3] = 1
				new_point = R.dot(point_vector)
				point.x = new_point[0]
				point.y = new_point[1]
				point.z = new_point[2] + 0.15
				points.append(point)
				z += 0.1
			z = bz
			y += 0.1
		return points


	def detect_objects(self, pc, img):
		""" 
			Callback function for synchronised pointcloud and image messages.
			Object detections are matched with segmented objects. This list of
			ExtendedCluster objects are then passed to self.pick_up.

			We wait for the pick up action to executed and if succesful then we 
			execute self.place(). Else we resume detecting objects and attempting
			to pick and place.

			Arguments:
				sensor_msgs/PointCloud2: pointcloud of the environment from RGBD camera
				sensor_msgs/Image: image of environment from RGBD camera
		"""
		#convert image to cv2 format
		try:
			cv_img = self.bridge.imgmsg_to_cv2(img, 'bgr8')
		except CvBridgeError as e:
			return

		detected_objects = self.yolo(img, "coco", 0.5, 0.1).detected_objects
		clusters = self.segment_objects(pc, 0.2, 0.035, 0.4).clusters
		objects = self.get_objects(clusters, detected_objects, pc.width, cv_img)

		ros_img = self.bridge.cv2_to_imgmsg(cv_img)
		self.detection_pub.publish(ros_img)

		if self.grasping == True:
			rospy.loginfo("Pickup result: {0}".format(self.pick_as.get_state()))
			if self.pick_as.get_state() == GoalStatus.SUCCEEDED:
				result = self.pick_as.get_result()
				if str(moveit_error_dict[result.error_code]) != "SUCCESS":
					self.grasping = False
				else:
					for sub in self.detection_subs:
						sub.unregister()
					self.grasping = False
					self.pick_as.stop_tracking_goal()
					self.place(self.place_table)
			else:
				self.grasping = False
				self.pick_as.stop_tracking_goal()
				return
		else:
			self.pick_up(objects)


	def update_planes(self):
		"""
			Waits for next pointcloud message and detects planes present
			within that pointcloud, and updates self.planes. 
		"""
		pc = rospy.wait_for_message("/xtion/depth_registered/points", PointCloud2)
		self.planes = self.detect_planes(pc, 0.2).planes


	def get_table_plane(self):
		"""
			Finds the upward facing plane closest to the approximate position of
			a table (position defined by approx_table).

			Returns:
				Plane: plane closest to approximate table position
		"""
		planes = self.get_upward_facing_planes()

		#approximate table position for finding the support surface
		approx_table = Point()
		approx_table.x = 0.5
		approx_table.y = 0
		approx_table.z = 0.8

		#find plane most likely to be table according to approximate table position
		table_plane = None
		min_dist = 1000
		id = 0
		for plane in planes:
			if self.euclidean_dist(plane.pose.position, approx_table) < min_dist:
				min_dist = self.euclidean_dist(plane.pose.position, approx_table)
				table_plane = plane
			id += 1

		return table_plane


	def place(self, table):
		"""
			First we run object segmentation and object detection to generate collision
			boxes, and determine what class each object has. We match detections to
			object clusters.

			We then generate a set of points on the storage surface and sort them by score.
			Then iterate through each point until a plan is found. If no placing plan is found
			the place the object back on the pick up surface.

			Arguments:
				geometry_msgs/PoseStamped: pose at which to generate placings
		"""
		rospy.loginfo("Placing object.")

		self.movement_handler.move_to_pose(table)
		self.movement_handler.lift_torso()
		self.movement_handler.lower_head()

		collision_objects = []

		#get planes
		self.update_planes()

		table_plane = self.get_table_plane()

		collision_objects.append(self.make_collision_box_from_plane(table_plane, id))

		#get object clusters on table
		pc = rospy.wait_for_message("/xtion/depth_registered/points", PointCloud2)
		objects = self.segment_objects(pc, 0.2, 0.035, 0.4).clusters

		#detect objects on table
		# for i in range(0, 5):
		img = rospy.wait_for_message("/xtion/rgb/image_rect_color", Image)
		detected_objects = self.yolo(img, "coco", 0.5, 0.1).detected_objects

		matched_objects = []
		#match clusters and detections
		for idx, cluster in enumerate(objects):
			matched = False
			for detection in detected_objects:
				overlap = self.rect_overlap_degree(
					self.bounding_box_from_cluster(cluster, pc.width), 
					self.bounding_box_from_detection(detection))
				if overlap > 0.6:
					matched = True
					matched_objects.append(ExtendedCluster(cluster,
						detection.name, str(idx) + "_detection.name"))
			
			if matched == False:
				matched_objects.append(ExtendedCluster(cluster, "unknown", str(idx) + "_unknown"))

		rem_objects = []
		#add collision objects for objects on surface
		for idx in range(0, len(matched_objects)):
			rospy.loginfo("class id: {0}".format(matched_objects[idx].class_id))
			test_centroid = []
			test_centroid.append(matched_objects[idx].cluster.pose.position)
			place_points = self.on_surface(table_plane, test_centroid, 0.5).surface_points
			#if centroid is on table plane add to scene
			if len(place_points) > 0:
				collision_objects.append(self.make_collision_box(matched_objects[idx].cluster.pose,
					matched_objects[idx].cluster.size, "base_footprint", matched_objects[idx].name))
			else:
				rem_objects.append(matched_objects[idx])

		#remove objects not on table
		for obj in rem_objects:
			matched_objects.remove(obj)

		#approx_table should be made into node parameter that can be set in launch file!!!!!!!!!!!!!
		approx_table = Point()
		approx_table.x = 0.5
		approx_table.y = 0
		approx_table.z = 0.8

		ko = Bool()
		ko.data = True
		self.setup_scene(collision_objects, ko, approx_table)

		#generate place points
		place_points = self.gen_place_points(table_plane)
		place_points = [point for point in place_points if point.x < 1.3] #remove points too far from robot
		place_points = self.on_surface(table_plane, place_points, 0.5).surface_points
		place_points = self.sort_points(place_points, table_plane, matched_objects)

		#for each point attempt to plan and execute an accepting place action
		for point in place_points:
			pose = PoseStamped()
			pose.header.frame_id = "base_footprint"
			pose.header.stamp = rospy.Time.now()
			pose.pose.position = point
			pose.pose.orientation.w = 1
			place_g = ExecPlaceGoal()
			place_g.placings = self.sp(pose).placings
			self.place_as.send_goal_and_wait(place_g)
			result = self.place_as.get_result()
			if str(moveit_error_dict[result.error_code]) == "SUCCESS":
				#start moving back to the pick table
				self.movement_handler.prepare_robot()
				self.movement_handler.move_to_pose(self.pick_table)
				self.update_planes()

				#reset detection subscribers for picking up next object
				self.pc_sub = message_filters.Subscriber('/xtion/depth_registered/points', PointCloud2)
				self.img_sub = message_filters.Subscriber('/xtion/rgb/image_rect_color', Image)
				self.detection_subs = [self.pc_sub, self.img_sub]
				self.sync = message_filters.TimeSynchronizer(self.detection_subs, 1)
				self.sync.registerCallback(self.detect_objects)
				return

		#if couldnt find a valid plan then place back on the pick table
		if str(moveit_error_dict[result.error_code]) == "PLANNING_FAILED":
			self.place(self.pick_table)
		
		#start moving back to the pick table
		self.movement_handler.prepare_robot()
		self.movement_handler.move_to_pose(self.pick_table)
		self.update_planes()


	def pick_up(self, objects):
		"""
			Iterates over each object on the table, creates a list of potential grasps
			to attempt to execute. Collision boxes for each object on the table are added
			to the planning scene.

			If no grasp can be executed then we return to the detection loop. If a grasp
			is succesfully executed then we set self.current_object to the grasped objects
			class_id.

			Arguments:
				ExtendedCluster[]: list of objects percieved within the scene
		"""
		rospy.loginfo("Attempting pickup.")

		if len(objects) == 0:
			return

		grasp_object = None

		table_plane = self.get_table_plane()

		objects = self.filter_plane_objects(objects, table_plane)

		for i in range(0, len(objects)):
			grasp_object = objects[i]
			collision_objects = []

			if grasp_object.class_id != "unknown" or self.ignore_class:
				#add collision objects for other known and unknown objects not being picked up
				for idx in range(0, len(objects)):
					if idx != i:
						collision_objects.append(self.make_collision_box(objects[idx].cluster.pose,
							objects[idx].cluster.size, "base_footprint", objects[idx].name))
					else:
						#add collision object for object to be picked up
						collision_objects.append(self.make_collision_box(grasp_object.cluster.pose,
							grasp_object.cluster.size, "base_footprint", "object"))


				#add collision box for table plane
				collision_objects.append(self.make_collision_box_from_plane(table_plane, 0))

				rospy.loginfo("Adding {0} collision objects to the planning scene.".format(len(collision_objects)))

				ko = Bool()
				ko.data = False
				self.setup_scene(collision_objects, ko, grasp_object.cluster.pose.position)
				self.grasping = True

				pick_g = ExecGraspGoal()
				pick_g.object_pose.pose.position = grasp_object.cluster.pose.position
				pick_g.object_pose.header.frame_id = grasp_object.cluster.header.frame_id
				pick_g.object_pose.pose.orientation.w = 1
				pick_g.grasps.extend(self.sg(pick_g.object_pose, grasp_object.cluster.size).grasps)

				self.pick_as.send_goal_and_wait(pick_g)
				result = self.pick_as.get_result()

				rospy.loginfo("Grasp resulton {0}: {1}".format(grasp_object.class_id, result.error_code))
				
				self.movement_handler.prepare_robot()
				if str(moveit_error_dict[result.error_code]) != "SUCCESS":
					rospy.loginfo("Preparing to grasp next object.")
				else:
					self.current_object = grasp_object.class_id
					self.movement_handler.raise_head()
					break


	def make_collision_box(self, pose, size, frame, id):
		"""
			Makes a moveit_msgs/CollisionObject of specified dimensions with
			fixed pose.

			Arguments:
				geometry_msgs/Pose: pose containing position for box
				geometry_msgs/Point: point where dimensional values represent
				side lengths
				string: the frame in which the collision object is defined
				string: name of corresponding ExtendedCluster

			Returns:
				moveit_msgs/CollisionObject: collision object for plane to add
				to planning scene
		"""
		collision_object = CollisionObject()
		collision_object.header.frame_id = frame
		collision_object.id = str(id)

		shape = SolidPrimitive()
		pose_t = Pose()

		shape.type = shape.BOX
		shape.dimensions = [None]*3
		shape.dimensions[shape.BOX_X] = size.x
		shape.dimensions[shape.BOX_Y] = size.y
		shape.dimensions[shape.BOX_Z] = size.z

		pose_t.position.x = pose.position.x
		pose_t.position.y = pose.position.y
		pose_t.position.z = pose.position.z
		pose_t.orientation.w = 1


		collision_object.primitives.append(shape)
		collision_object.primitive_poses.append(pose_t)
		collision_object.operation = collision_object.ADD

		return collision_object

	def make_collision_box_from_plane(self, plane, id):
		"""
			Makes a moveit_msgs/CollisionObject for a given plane. This object
			consists of a single box mesh.

			Arguments:
				Plane: plane for which to make a collision object
				int: numerical id for collision object

			Returns:
				moveit_msgs/CollisionObject: collision object for plane to add
				to planning scene
		"""
		collision_object = CollisionObject()
		collision_object.header.frame_id = "base_footprint"
		collision_object.id = str(id) + "_plane"

		shape = SolidPrimitive()
		shape.type = shape.BOX
		shape.dimensions = [None]*3
		shape.dimensions[shape.BOX_X] = plane.pose.position.z + abs(plane.size.x/2) - 0.008
		shape.dimensions[shape.BOX_Y] = plane.size.y + 0.05 #pad to prevent clipping corners
		shape.dimensions[shape.BOX_Z] = plane.size.z + 0.05

		table_pose = Pose()
		table_pose.position.x = plane.pose.position.x
		table_pose.position.y = plane.pose.position.y
		table_pose.position.z = plane.pose.position.z
		table_pose.orientation.x = plane.pose.orientation.x
		table_pose.orientation.y = plane.pose.orientation.y
		table_pose.orientation.z = plane.pose.orientation.z
		table_pose.orientation.w = plane.pose.orientation.w
		table_pose.position.z = table_pose.position.z/2

		collision_object.primitives.append(shape)
		collision_object.primitive_poses.append(table_pose)
		collision_object.operation = collision_object.ADD

		return collision_object

	def filter_plane_objects(self, objects, plane):
		"""
			Filters objects whose bounding boxes are bellow a specified size, and those
			with a centroid not on the planes surface (as calculated by plane_detections
			on_plane service)

			Aruments:
				ExtendedCluster[]: list of objects in the environment to filter
				Plane: the plane on which items should not be filtered

			Returns:
				ExtendedCluster[]: list of objects above threshold size and on plane 
		"""
		filtered_objects = []
		large_objects = []
		#filter small objects
		small_objs = 0
		for object in objects:
			if (abs(object.cluster.size.x) < 0.03 or 
			(abs(object.cluster.size.y) < 0.045 and abs(object.cluster.size.z) < 0.045)):
				small_objs += 1
			else:
				large_objects.append(object)
		rospy.loginfo("Removed {0} small clusters from segmentation result.".format(small_objs))

		#filter off plane objects
		off_plane = 0
		for object in large_objects:
			test_centroid = []
			test_centroid.append(object.cluster.pose.position)
			place_points = self.on_surface(plane, test_centroid, 0.5).surface_points
			if len(place_points) > 0:
				filtered_objects.append(object)
			else:
				off_plane += 1
		rospy.loginfo("Removed {0} clusters not on plane.".format(off_plane))

		return filtered_objects

	def get_upward_facing_planes(self):
		"""
			Returns a list of the planes in self.planes with normal parallel
			to the z axis.

			Returns:
				Planes[]: planes in self.planes that have a normal parallel to z axis
		"""
		upwards_planes = []
		for plane in self.planes:
			if(self.in_range(-0.03, 0.03, plane.normal[0]) and
			self.in_range(-0.03, 0.03, plane.normal[1]) and
			self.in_range(0.93, 1.07, plane.normal[2])):
				upwards_planes.append(plane)
		return upwards_planes


	def in_range(self, lb, ub, x):
		"""
			Determines if value x falls between lower and upper bounds.

			Arguments:
				float: lower bound
				float: upper bound
				float: value to check

			Returns:
				boolean: if x is in range return true, else false 
		"""
		if lb <= x <= ub:
			return True
		else:
			return False


	def get_objects(self, clusters, detected_objects, pc_width, cv_img):
		"""
			Matches bounding boxes of yolo detections with those computed from a cluster.
			Updates image with bounding boxes and class_ids.

			Arguments:
				jeff_segment_objects/Cluster[]: clusters extracted from pointcloud
				lasr_object_detection_yolo/Detection[]: yolo object detections
				int: width of the pointcloud
				CV2Image: image of environment
			
			Returns:
				ExtendedCluster[]: clusters present in scene with matched class_ids
		"""
		objects = []
		marker_id = 1
		for idx, cluster in enumerate(clusters):
			cluster_bb = True

			#get the bounding box for the cluster
			bbox_cluster = self.bounding_box_from_cluster(cluster, pc_width)
			c_left = bbox_cluster[0]
			c_bottom = bbox_cluster[1]
			c_right = bbox_cluster[2]
			c_top = bbox_cluster[3]

			#compare bounding box against yolo object detection to determine
			#which pc refers to which object
			for detection in detected_objects:
				bbox_detection = self.bounding_box_from_detection(detection)
				o_left = bbox_detection[0]
				o_bottom = bbox_detection[1]
				o_right = bbox_detection[2]
				o_top = bbox_detection[3]

				#get IoU of segmentation bb and detection bb
				overlap = self.rect_overlap_degree([c_left, c_bottom, c_right, c_top], [o_left, o_bottom, o_right, o_top])
				threshold = 0.6

				if overlap >= threshold:
					cv2.rectangle(cv_img, (o_left, o_bottom), (o_right, o_top), (0,0,255), 2)
					label = "{}: {:.4f}".format(detection.name, detection.confidence)
					cv2.putText(cv_img, label, (o_left, o_bottom - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 2)
					objects.append(ExtendedCluster(cluster, detection.name, detection.name + "_" + str(idx)))
					cluster_bb = False
					break

			#if no detected object matched draw bounding box for cluster and add unknown object
			if cluster_bb:
				objects.append(ExtendedCluster(cluster, "unknown", "unknown_" + str(idx)))
				cv2.rectangle(cv_img, (c_left, c_bottom), (c_right, c_top), (0,255,0), 2)
				label = "No match"
				cv2.putText(cv_img, label, (c_left, c_bottom - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)

		return objects

	def rect_overlap_degree(self, rec1, rec2):
		"""
			Returns the Intersect over Union (IoU) of two rectangles

			Arguments:
				[left, bottom, right, top]: rectangle 1
				[left, bottom, right, top]:	rectangle 2
			
			Returns:
				float: IoU
		"""
		dx = min(rec1[2], rec2[2]) - max(rec1[0], rec2[0])
		dy = min(rec1[3], rec2[3]) - max(rec1[1], rec2[1])
		if (dx>=0) and (dy>=0):
			rec1_area = (rec1[2]-rec1[0])*(rec1[3]-rec1[1])
			rec2_area = (rec2[2]-rec2[0])*(rec2[3]-rec2[1])
			overlap_area = dx*dy
			return float(overlap_area)/float((rec1_area + rec2_area - overlap_area))
		else:
			return 0.0
	

	def bounding_box_from_cluster(self, cluster, pc_width):
		"""
			Returns the bottom left and top right corners for the 2D bounding box
			of a given cluster

			Arguments:
				jeff_segment_objects/Cluster: cluster to calculate 2D bounding box for
				int: width of the pointcloud

			Returns:
				[left, bottom, right, top]: 2D bounding box of the cluster
		"""
		c_right = cluster.indices[0]%pc_width
		c_left = cluster.indices[0]%pc_width
		c_top = int(cluster.indices[0]/pc_width)
		c_bottom = int(cluster.indices[0]/pc_width)
		for index in cluster.indices:
			#get the bounding box for the cluster
			row = int(index/pc_width)
			col = index%pc_width
			c_left = min(c_left, col)
			c_right = max(c_right, col)
			c_bottom = min(c_bottom, row)
			c_top = max(c_top, row)

		return [c_left, c_bottom, c_right, c_top]


	def bounding_box_from_detection(self, detection):
		"""
			Returns the bottom left and top right corners for the 2D bounding box
			defined by a yolo detection 

			Arguments:
				lasr_object_detection_yolo/Detection: detection to compute 2D bounding box for

			Returns:
				[left, bottom, right, top]: Bounding box
		"""
		o_left = int(detection.xywh[0])
		o_bottom = int(detection.xywh[1])
		o_right = int(o_left + detection.xywh[2])
		o_top = int(detection.xywh[1] + detection.xywh[3])

		return [o_left, o_bottom, o_right, o_top]
	

	def vis_point(self, point, id, colour, score):
		"""
			Publishes two visualization_msgs/Marker messages to self.marker_pub.
			Used for visualising placing points and associated score.

			Arguments:
				sensor_msgs/Point: place location to visualise
				int: Marker id
				[r,g,b]: colour of the sphere and corresponding text (each val range from 0 - 1)
				int: score corresponding to place location
		"""
		vis_point = Marker()
		vis_point.header.frame_id = "base_footprint"
		vis_point.header.stamp = rospy.Time.now()
		vis_point.id = id
		vis_point.ns = "grocery_storage"
		vis_point.type = Marker.SPHERE
		vis_point.action = Marker.ADD
		vis_point.pose.position.x = point.x
		vis_point.pose.position.y = point.y
		vis_point.pose.position.z = point.z
		vis_point.pose.orientation.x = 0
		vis_point.pose.orientation.y = 0
		vis_point.pose.orientation.z = 0
		vis_point.pose.orientation.w = 0
		scale = 0.05
		vis_point.scale.x = scale
		vis_point.scale.y = scale
		vis_point.scale.z = scale
		vis_point.color.r = colour[0]
		vis_point.color.g = colour[1]
		vis_point.color.b = colour[2]
		vis_point.color.a = 1.0
		vis_point.lifetime = rospy.Duration()
		
		self.marker_pub.publish(vis_point)
		# vis_text = vis_point
		# vis_text.id = id - 500
		# vis_text.type = Marker.TEXT_VIEW_FACING
		# vis_text.pose.position.z += 0.15
		# vis_text.scale.x = 0.02
		# vis_text.scale.y = 0.02
		# vis_text.scale.z = 0.02
		# vis_text.text = str(score)[:-6]
		# self.marker_pub.publish(vis_text)


# Used for grouping cluster data
class ExtendedCluster():
	def __init__(self, cluster, class_id, name = ""):
		self.cluster = cluster
		self.class_id = class_id
		self.position = cluster.pose.position
		self.name = name

# Used for sorting points by associated score
class SortPoint():
	def __init__(self, point, score):
		self.point = point
		self.score = score

def main(args):
    rospy.init_node('grocery_storage_handler', anonymous = True)
    gh = GroceriesHandler()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

if __name__ == '__main__':
    main(sys.argv)
