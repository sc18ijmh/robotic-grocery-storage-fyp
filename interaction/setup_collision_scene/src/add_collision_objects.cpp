// ROS
#include <ros/ros.h>

// MoveIt
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include <setup_collision_scene/SetupCollision.h>

//returns euclidean distance between two 3D points
float euclideanDistance(float x1, float y1, float z1, float x2, float y2, float z2){
    return sqrt(pow((x1-x2),2.0)+pow((y1-y2),2.0)+pow((z1-z2),2.0));
}


//Returns the id of the collision object closest to a given point.
std::string getSupportSurfaceName(std::vector<moveit_msgs::CollisionObject> collision_objects,
    geometry_msgs::Point point){
    //find support surface
    float dist = 1000;
    std::string support_id = "";
    for(int i = 0; i < collision_objects.size(); ++i){
        std::string is_plane = "_plane";
        if(collision_objects[i].id.find(is_plane) != std::string::npos){
            float x1 = point.x;
            float y1 = point.y;
            float z1 = point.z;
            float x2 = collision_objects[i].primitive_poses[0].position.x;
            float y2 = collision_objects[i].primitive_poses[0].position.y;
            float z2 = collision_objects[i].primitive_poses[0].position.z;
            if(euclideanDistance(x1, y1, z1, x2, y2, z2) < dist){
                dist = euclideanDistance(x1, y1, z1, x2, y2, z2);
                support_id = collision_objects[i].id;
            }
        }
    }
    return support_id;
}

/*
Clears all collision objects from planning scene, then adds those present in the request.
If Keep Object == True then dont remove collision box with id == "object"
*/
bool setupCollisionObjects(setup_collision_scene::SetupCollision::Request &req,
    setup_collision_scene::SetupCollision::Response &res){

    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    moveit::planning_interface::MoveGroupInterface group("arm");

    //remove prior collision_objects
    std::map<std::string, moveit_msgs::CollisionObject> objects_map= planning_scene_interface.getObjects();
    std::map<std::string, moveit_msgs::CollisionObject>::iterator it = objects_map.begin();
    std::vector<std::string> rem_collision_object_ids;
    while(it != objects_map.end()){
        std::string obj_id = it->first;
        if(obj_id.find("object") != std::string::npos && req.keep_object.data){
            //do nothing
        }else{
            std::cout << obj_id << std::endl;
            rem_collision_object_ids.push_back(obj_id);
        }
        it++;
    }
    planning_scene_interface.removeCollisionObjects(rem_collision_object_ids);

    //add new collision objects
    planning_scene_interface.addCollisionObjects(req.collision_objects);

    group.setSupportSurfaceName(getSupportSurfaceName(req.collision_objects, req.centroid));
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "detect_planes");
    ros::NodeHandle n;
    ros::ServiceServer add_collision_objects = n.advertiseService("setup_collision_scene", setupCollisionObjects);
    ros::spin();
}
