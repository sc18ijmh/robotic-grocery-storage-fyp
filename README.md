## About this project

This repository contains all of the packages implemented as part of the solution to the grocery storage test.

## Dependencies

To use this project the lasr_object_detection_yolo (https://gitlab.com/sensible-robots/lasr_object_detection_yolo) package is required. Access to this package can be gained with permission from the Leeds Autonomous Service Robotics (LASR) team.

## Installation

To use the project you will need to install ROS and ROS TIAGo linked below.

- http://wiki.ros.org/kinetic/Installation/Ubuntu
- http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation


To install the project for use on your machine first clone the repository.

```
cd ~<catkin-workspace-root>/src
git clone https://gitlab.com/sc18ijmh/robotic-grocery-storage-fyp.git grocery_storage
```

## Building

To build the project navigate into the root of the catkin workspace and run `catkin build`.
```
cd <catkin-workspace-root>
catkin build
```

## Usage

### Setup World and Map (Optional)
1. Before executing the code there is some setup required. First a map of the environment should be created using gmapping (for a gazeo world file `roslaunch tiago_2dnav_gazebo tiago_mapping.launch public_sim:=true` launches the gmapping service). 

    - Open a terminal and run `rosrun key_teleop key_teleop.py` to move the robot around the environment
    - When satisfied with the state of the map (visualise in rviz) run `rosservice call /pal_map_manager/save_map "directory: ''"` to save the map file

2. The solution is not fully autonomous and as such poses for the base_footprint frame of the robot must be defined in the grocery_storage/config/table_poses.yaml file for locating the pick and place surfaces.

3. Now load you world file in gazebo, and launch your localisation script with your new map.

### Launch grocery_storage project
All that is left is to run `roslaunch grocery_storage grocery_storage.launch` which will spin the following nodes
- plane_detection_server
- jeff_segment_objects_node
- simple_grasp_estimation
- execute_grasp_server
- object_detection_server
- grocery_storage

The robot should start by planning to the pick up table pose specified in the table_poses.yaml file, pick up recognised objects and then place them on the table at the place table pose.
