#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>

#include <plane_detection/DetectPlanes.h>
#include <plane_detection/OnSurface.h>
#include <plane_detection/Plane.h>
#include <plane_detection/MergePcs.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/surface/convex_hull.h>
#include <iostream>

tf::TransformListener* tf_listener;
ros::Publisher marker_pub;
ros::Publisher marker_pub2;
ros::Publisher pcl_pub;
// map inliers from a cloud filter to their root indices
// kept indices can thus be traced back to the root cloud after applying multiple filters
//
void mapInliersToRootIndices(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, pcl::PointIndices::Ptr inliers, pcl::PointIndices::Ptr root_indices)
{
    int i = 0;
    pcl::PointIndices::Ptr temp_indices (new pcl::PointIndices);
    for(int j = 0; j < cloud->size(); ++j)
    {
        while(inliers->indices[i] < j && i < inliers->indices.size())
            ++i;

        if(j != inliers->indices[i])
            temp_indices->indices.push_back(root_indices->indices[j]);
    }
    root_indices->indices = temp_indices->indices;
}


//returns positive anticlockwise angle from v1 to v2
float angleTo2DVector(float x1, float y1, float x2, float y2){
    float angle1, angle2, delta;
    angle1 = atan2(y1, x1);
    angle2 = atan2(y2, x2);
    delta = angle2 - angle1;
    while(delta > M_PI){
        delta -= 2*M_PI;
    }
    while(delta < -M_PI){
        delta += 2*M_PI;
    }
    return delta;
}


bool insideHull(pcl::PointCloud<pcl::PointXYZ>::Ptr hull, pcl::Vertices polygon, float x, float y){
    float total_angle = 0;
    float x1, y1, x2, y2;
    for(int i = 0; i < polygon.vertices.size() - 1; ++i){

        x1 = hull->points[polygon.vertices[i]].x - x;
        y1 = hull->points[polygon.vertices[i]].y - y;
        x2 = hull->points[polygon.vertices[i+1]].x - x;
        y2 = hull->points[polygon.vertices[i+1]].y - y;
        total_angle += angleTo2DVector(x1, y1, x2, y2);
    }

    //loop back to begining of vertices
    x1 = hull->points[polygon.vertices[polygon.vertices.size() - 1]].x - x;
    y1 = hull->points[polygon.vertices[polygon.vertices.size() - 1]].y - y;
    x2 = hull->points[polygon.vertices[0]].x - x;
    y2 = hull->points[polygon.vertices[0]].y - y;
    total_angle += angleTo2DVector(x1, y1, x2, y2);


    if(total_angle < 0){
        total_angle *= -1;
    }

    if(total_angle < M_PI)
        return false;
    else
        return true;
}

sensor_msgs::PointCloud2 getVoxelSurface(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr plane_cloud){
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr voxel_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
    //make voxel grid
    pcl::VoxelGrid<pcl::PointXYZRGBA> vox;
    vox.setInputCloud(plane_cloud);
    vox.setLeafSize(0.1f, 0.1f, 0.1f);
    vox.filter(*voxel_cloud);

    sensor_msgs::PointCloud2 voxel_surface;
    pcl::toROSMsg(*voxel_cloud, voxel_surface);
    voxel_surface.header.frame_id = "base_footprint";
    voxel_surface.header.stamp = ros::Time::now();
    return voxel_surface;
}


bool detectPlanes(plane_detection::DetectPlanes::Request &req, plane_detection::DetectPlanes::Response &res){
    ROS_INFO("Detecting planes.");
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_msg(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointIndices::Ptr root_indices(new pcl::PointIndices);

    //convert req PointCloud2 to pcl::PointCloud
    pcl::fromROSMsg(req.cloud_msg, *cloud_msg);
    std::cout << req.cloud_msg.header.frame_id << std::endl;
    if(req.cloud_msg.header.frame_id != "base_footprint"){
        //transform pointcloud to base_frame so planes are orthogonal to axes
        pcl_ros::transformPointCloud("base_footprint", *cloud_msg, *cloud, *tf_listener);
    }else{
        cloud = cloud_msg;
    }

    int cloud_msg_size = cloud->size();

    //remove nans from cloud
    std::vector<int> nans;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nans);

    std::cout << "cloud size: " << cloud->size() << " inlier size: " << nans.size() << " filtered: " << nan_filtered_cloud->size() << std::endl;

    cloud.swap(nan_filtered_cloud);
    root_indices->indices = nans;

    //get plane coefficients
    std::vector<pcl::ModelCoefficients> plane_coefficients;

    //plane segmentation
    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setMaxIterations(1000);
    seg.setDistanceThreshold(0.01);

    // segment planes from cloud
    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);

    std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> plane_clouds;

    // create plane cloud
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract(true);
    extract.setInputCloud(cloud);
    extract.setIndices(inliers);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    extract.setNegative(true);
    extract.filter(*filtered_cloud);
    pcl::IndicesConstPtr indices = extract.getRemovedIndices();

    //construct plane pointcloud
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr plane_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    for (std::vector<int>::const_iterator point_it = inliers->indices.begin(); point_it != inliers->indices.end (); ++point_it) {
        plane_cloud->points.push_back(cloud->points[*point_it]);
    }
    plane_cloud->width = plane_cloud->size();
    plane_cloud->height = 1;
    plane_cloud->is_dense = true;

    std::cout << "Detected plane of size: " << inliers->indices.size() << std::endl;

    // map to root indices
    mapInliersToRootIndices(cloud, inliers, root_indices);
    cloud.swap(filtered_cloud);

    plane_clouds.push_back(plane_cloud);
    pcl::ModelCoefficients coef = *coefficients;
    plane_coefficients.push_back(coef);

    //repeat until all planes have been filtered from the cloud
    while(cloud->size() > cloud_msg_size * req.non_planar_ratio){
        seg.setInputCloud(cloud);
        seg.segment(*inliers, *coefficients);

        // create plane cloud
        pcl::ExtractIndices<pcl::PointXYZRGBA> extract(true);
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        extract.setNegative(true);
        extract.filter(*filtered_cloud);
        pcl::IndicesConstPtr indices = extract.getRemovedIndices();

        //construct plane pointcloud
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr plane_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        for (std::vector<int>::const_iterator point_it = inliers->indices.begin(); point_it != inliers->indices.end (); ++point_it) {
            plane_cloud->points.push_back(cloud->points[*point_it]);
        }
        plane_cloud->width = plane_cloud->size();
        plane_cloud->height = 1;
        plane_cloud->is_dense = true;
        std::cout << "Detected plane of size: " << inliers->indices.size() << std::endl;

        // map to root indices
        mapInliersToRootIndices(cloud, inliers, root_indices);
        cloud.swap(filtered_cloud);
        plane_clouds.push_back(plane_cloud);
        pcl::ModelCoefficients coef = *coefficients;
        plane_coefficients.push_back(coef);
    }
    int bb_id = 0;
    //iterate through plane clouds
    for(int i = 0; i < plane_clouds.size(); ++i){
        // extract seperated clusters within the plane cloud so that individual
        // clusters are recognised as seperate entities within the scene

        // create a tree for cluster extraction
        pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
        tree->setInputCloud(plane_clouds[i]);

        // cluster extraction
        pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;
        std::vector<pcl::PointIndices> sub_plane_indices;
        ec.setClusterTolerance(0.035);
        ec.setMinClusterSize(30000); //we are interested in large planes (such as table/walls)
        ec.setMaxClusterSize(250000);
        ec.setSearchMethod(tree);
        ec.setInputCloud(plane_clouds[i]);
        ec.extract(sub_plane_indices);
        std::cout << "For plane " << i << " there are " << sub_plane_indices.size() << " clusters." << std::endl;


        //iterate through sub plane clouds
        for(std::vector<pcl::PointIndices>::const_iterator it = sub_plane_indices.begin(); it != sub_plane_indices.end (); ++it){
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sub_plane_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
            for(std::vector<int>::const_iterator point_it = it->indices.begin(); point_it != it->indices.end(); ++point_it){
                sub_plane_cloud->push_back(plane_clouds[i]->points[*point_it]);
            }
            sub_plane_cloud->width = sub_plane_cloud->size();
            sub_plane_cloud->height = 1;
            sub_plane_cloud->is_dense = true;

            // code for oriented bounding boxes from below
            // https://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html
            Eigen::Vector4f pcaCentroid;
            pcl::compute3DCentroid(*sub_plane_cloud, pcaCentroid);

            Eigen::Matrix3f covariance;
            pcl::computeCovarianceMatrixNormalized(*sub_plane_cloud, pcaCentroid, covariance);
            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
            Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
            eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));  /// This line is necessary for proper orientation in some cases. The numbers come out the same without it, but
            // Transform the original cloud to the origin where the principal components correspond to the axes.
            Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
            projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
            projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * pcaCentroid.head<3>());
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZRGBA>);
            pcl::transformPointCloud(*sub_plane_cloud, *cloudPointsProjected, projectionTransform);
            // Get the minimum and maximum points of the transformed cloud.
            pcl::PointXYZRGBA minPoint, maxPoint;
            pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
            //std::cout << minPoint << ", " << maxPoint << std::endl;

            const Eigen::Vector3f meanDiagonal = 0.5f*(maxPoint.getVector3fMap() + minPoint.getVector3fMap());
            // Final transform
            const Eigen::Quaternionf bboxQuaternion(eigenVectorsPCA); //Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
            const Eigen::Vector3f bboxTransform = eigenVectorsPCA * meanDiagonal + pcaCentroid.head<3>();


            //create marker for visualising the bounding box in rviz
            visualization_msgs::Marker vis_pose;
            vis_pose.header.frame_id = "base_footprint";
            vis_pose.header.stamp = ros::Time::now();
            vis_pose.ns = "exec_grasp";
            vis_pose.id = bb_id;
            vis_pose.type = visualization_msgs::Marker::CUBE;
            vis_pose.action = visualization_msgs::Marker::ADD;
            vis_pose.pose.position.x = bboxTransform[0];
            vis_pose.pose.position.y = bboxTransform[1];
            vis_pose.pose.position.z = bboxTransform[2];
            vis_pose.pose.orientation.x = bboxQuaternion.x();
            vis_pose.pose.orientation.y = bboxQuaternion.y();
            vis_pose.pose.orientation.z = bboxQuaternion.z();
            vis_pose.pose.orientation.w = bboxQuaternion.w();
            vis_pose.scale.x = maxPoint.x - minPoint.x;
            vis_pose.scale.y = maxPoint.y - minPoint.y;
            vis_pose.scale.z = maxPoint.z - minPoint.z;
            vis_pose.color.r = 1.0f;
            vis_pose.color.a = 1.0;
            vis_pose.lifetime = ros::Duration();
            marker_pub.publish(vis_pose);

            //add plane to response
            plane_detection::Plane plane;
            plane.header.frame_id = "base_footprint";
            plane.header.stamp = ros::Time::now();
            plane.pose.position.x = bboxTransform[0];
            plane.pose.position.y = bboxTransform[1];
            plane.pose.position.z = bboxTransform[2];
            plane.pose.orientation.x = bboxQuaternion.x();
            plane.pose.orientation.y = bboxQuaternion.y();
            plane.pose.orientation.z = bboxQuaternion.z();
            plane.pose.orientation.w = bboxQuaternion.w();
            plane.size.x = maxPoint.x - minPoint.x;
            plane.size.y = maxPoint.y - minPoint.y;
            plane.size.z = maxPoint.z - minPoint.z;
            plane.normal.push_back(plane_coefficients[i].values[0]);
            plane.normal.push_back(plane_coefficients[i].values[1]);
            plane.normal.push_back(plane_coefficients[i].values[2]);
            plane.normal.push_back(plane_coefficients[i].values[3]);
            plane.voxel_surface = getVoxelSurface(sub_plane_cloud);
            res.planes.push_back(plane);
            ++bb_id;
        }
    }
    ROS_INFO_STREAM("Planes detected: " << bb_id);

    return true;
}

bool onPlane(plane_detection::OnSurface::Request &req, plane_detection::OnSurface::Response &res){
    pcl::PointCloud<pcl::PointXYZ>::Ptr voxel_surface_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(req.plane.voxel_surface, *voxel_surface_cloud);

    ROS_INFO_STREAM("Testing " << req.points.size() << " points.");

    // Create a Concave Hull representation of the projected inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr surface_hull(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ConvexHull<pcl::PointXYZ> chull;
    chull.setInputCloud (voxel_surface_cloud);
    chull.setDimension(2);
    //chull.setAlpha (0.1);
    std::vector<pcl::Vertices> polygons;
    chull.reconstruct (*surface_hull, polygons);
    pcl::PCDWriter writer;
    writer.write("plane_convex_hull.pcd", *surface_hull, false);

    std::cout << "No polygons: " << polygons.size() << std::endl;

    for(int i = 0; i < req.points.size(); ++i){
        geometry_msgs::Point req_point = req.points[i];

        bool in_z_range = false;
        if(req_point.z < (req.plane.pose.position.z + req.upper_bound) 
            && req_point.z > req.plane.pose.position.z){
            in_z_range = true;
        }

        if(insideHull(surface_hull, polygons[0], req_point.x, req_point.y) && in_z_range){
            res.surface_points.push_back(req_point);

            //create marker for point not added
            visualization_msgs::Marker vis_point;
            vis_point.header.frame_id = "base_footprint";
            vis_point.header.stamp = ros::Time::now();
            vis_point.ns = "exec_grasp";
            vis_point.id = i;
            vis_point.type = visualization_msgs::Marker::SPHERE;
            vis_point.action = visualization_msgs::Marker::ADD;
            vis_point.pose.position.x = req_point.x;
            vis_point.pose.position.y = req_point.y;
            vis_point.pose.position.z = req_point.z;
            vis_point.pose.orientation.x = 0;
            vis_point.pose.orientation.y = 0;
            vis_point.pose.orientation.z = 0;
            vis_point.pose.orientation.w = 1;
            vis_point.scale.x = 0.05;
            vis_point.scale.y = 0.05;
            vis_point.scale.z = 0.05;
            vis_point.color.g = 1.0f;
            vis_point.color.a = 1.0;
            vis_point.lifetime = ros::Duration();
            marker_pub2.publish(vis_point);
        }else{
            //create marker for point not added
            visualization_msgs::Marker vis_point;
            vis_point.header.frame_id = "base_footprint";
            vis_point.header.stamp = ros::Time::now();
            vis_point.ns = "exec_grasp";
            vis_point.id = i;
            vis_point.type = visualization_msgs::Marker::SPHERE;
            vis_point.action = visualization_msgs::Marker::ADD;
            vis_point.pose.position.x = req_point.x;
            vis_point.pose.position.y = req_point.y;
            vis_point.pose.position.z = req_point.z;
            vis_point.pose.orientation.x = 0;
            vis_point.pose.orientation.y = 0;
            vis_point.pose.orientation.z = 0;
            vis_point.pose.orientation.w = 1;
            vis_point.scale.x = 0.05;
            vis_point.scale.y = 0.05;
            vis_point.scale.z = 0.05;
            vis_point.color.r = 1.0f;
            vis_point.color.a = 1.0;
            vis_point.lifetime = ros::Duration();
            marker_pub2.publish(vis_point);
        }
    }
    ROS_INFO_STREAM("Found " << res.surface_points.size() << " on plane.");


    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "detect_planes");
    ros::NodeHandle n;
    marker_pub = n.advertise<visualization_msgs::Marker>("my_marker", 1000);
    marker_pub2 = n.advertise<visualization_msgs::Marker>("point_marker", 1000);
    pcl_pub = n.advertise<sensor_msgs::PointCloud2>("my_pcl", 100, true);
    ros::Duration(2).sleep();
    tf_listener = new tf::TransformListener();
    ros::ServiceServer detection_service = n.advertiseService("detect_planes", detectPlanes);
    ros::ServiceServer on_surface_service = n.advertiseService("point_on_plane", onPlane);

    std::cout << "Plane segmentation server ready!" << std::endl;
    ros::spin();
}
