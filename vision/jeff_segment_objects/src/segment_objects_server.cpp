#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Point.h>

#include <jeff_segment_objects/Cluster.h>
#include <jeff_segment_objects/SegmentObjects.h>
#include <jeff_segment_objects/RemoveBox.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/conditional_removal.h>

#include <iostream>

// transform listener to project objects onto base_footprint
tf::TransformListener* listener;

// map inliers from a cloud filter to their root indices
// kept indices can thus be traced back to the root cloud after applying multiple filters
//
void mapInliersToRootIndices(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, pcl::PointIndices::Ptr inliers, pcl::PointIndices::Ptr root_indices)
{
    int i = 0;
    pcl::PointIndices::Ptr temp_indices (new pcl::PointIndices);
    for(int j = 0; j < cloud->size(); ++j)
    {
        while(inliers->indices[i] < j && i < inliers->indices.size())
            ++i;

        if(j != inliers->indices[i])
            temp_indices->indices.push_back(root_indices->indices[j]);
    }
    root_indices->indices = temp_indices->indices;
}

// cloud and image callback always running to reduce overhead of new subscribers
//
bool segment_objects(jeff_segment_objects::SegmentObjects::Request &req, jeff_segment_objects::SegmentObjects::Response &res)
{
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr xtion_frame_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr nan_filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointIndices::Ptr root_indices(new pcl::PointIndices);


    // convert ros cloud_msg input
    pcl::fromROSMsg(req.cloud_msg, *xtion_frame_cloud);
    //transform to base footprint to prevent pointcloud from being slanted if robot head at angle
    pcl_ros::transformPointCloud("base_footprint", *xtion_frame_cloud, *cloud, *listener);
    int initial_size = cloud->size();

    std::vector<int> nan_indices;
    pcl::removeNaNFromPointCloud(*cloud, *nan_filtered_cloud, nan_indices);

    std::cout << "cloud size: " << cloud->size() << " inlier size: " << nan_indices.size() << " filtered: " << nan_filtered_cloud->size() << std::endl;

    cloud.swap(nan_filtered_cloud);
    root_indices->indices = nan_indices;

    // plane segmentation
    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setMaxIterations(1000);
    seg.setDistanceThreshold(0.01);

    // segment planes from cloud
    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);
    // filter the segmented plane from cloud
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    extract.setInputCloud(cloud);
    extract.setIndices(inliers);
    extract.setNegative(true);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
    extract.filter(*filtered_cloud);

    std::cout << "cloud size: " << cloud->size() << " inliers size: " << inliers->indices.size() << " filtered: " << filtered_cloud->size() << std::endl;

    // map to root indices
    mapInliersToRootIndices(cloud, inliers, root_indices);
    cloud.swap(filtered_cloud);

    // extract planes until 80% of the pcl is removed
    while (cloud->size() > initial_size * req.non_planar_ratio)
    {
        // segment planes from cloud
        seg.setInputCloud(cloud);
        seg.segment(*inliers, *coefficients);
        // filter the segmented plane from cloud
        pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(true);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
        extract.filter(*filtered_cloud);

        std::cout << "cloud size: " << cloud->size() << " inliers size: " << inliers->indices.size() << " filtered: " << filtered_cloud->size() << std::endl;

        // map to root indices
        mapInliersToRootIndices(cloud, inliers, root_indices);
        cloud.swap(filtered_cloud);
    }
    std::cout << std::endl << std::endl;

    // create a tree for cluster extraction
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud(cloud);

    // cluster extraction
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;
    std::vector<pcl::PointIndices> cluster_indices;
    ec.setClusterTolerance(req.cluster_tolerance);
    ec.setMinClusterSize(100);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud);
    ec.extract(cluster_indices);

    // set response
    // for each PointIndices object, add all points to a Cluster
    // append the Cluster to response's clusters vector
    for (std::vector<pcl::PointIndices>::const_iterator cluster_it = cluster_indices.begin (); cluster_it!= cluster_indices.end (); ++cluster_it)
    {
        jeff_segment_objects::Cluster cluster;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster_cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);

        for (std::vector<int>::const_iterator point_it = cluster_it->indices.begin (); point_it != cluster_it->indices.end (); ++point_it) {
            cluster.indices.push_back(root_indices->indices[*point_it]);
            pcl::PointXYZRGBA point;
            point = cloud->points[*point_it];
            geometry_msgs::Point ros_point;
            ros_point.x = point.x;
            ros_point.y = point.y;
            ros_point.z = point.z;
            cluster.xyz.push_back(ros_point);
            cluster_cloud->points.push_back(cloud->points[*point_it]);
        }

        //setup cloud for 3D calculations
        cluster_cloud->width = cluster_cloud->points.size();
        cluster_cloud->height = 1;
        cluster_cloud->is_dense = true;

        // calculate object center point
        pcl::PointXYZRGBA object_centroid;
        pcl::computeCentroid(*cluster_cloud, object_centroid);
        cluster.header.stamp = req.cloud_msg.header.stamp;
        cluster.header.frame_id = "base_footprint";
        cluster.pose.position.x = object_centroid.x;
        cluster.pose.position.y = object_centroid.y;
        cluster.pose.position.z = object_centroid.z;
        cluster.pose.orientation.x = 0;
        cluster.pose.orientation.y = 0;
        cluster.pose.orientation.z = 0;
        cluster.pose.orientation.w = 1;

        // get size of object
        pcl::PointXYZRGBA object_min, object_max;
        pcl::getMinMax3D(*cluster_cloud, object_min, object_max);
        cluster.size.x = std::abs(object_max.x - object_min.x);
        cluster.size.y = std::abs(object_max.y - object_min.y);
        cluster.size.z = std::abs(object_max.z - object_min.z);


        //if gpd is in use then convert cluster cloud to Pointcloud2 and append to cluster
        if(req.return_cluster_cloud > 0){
            pcl::toROSMsg(*cluster_cloud, cluster.cloud);
        }

        res.clusters.push_back(cluster);
    }

    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "segment_objects");
    listener = new tf::TransformListener();
    ros::NodeHandle n;
    ros::Duration(2).sleep();

    ros::ServiceServer segment_service = n.advertiseService("segment_objects", segment_objects);
    std::cout << "Segment objects server ready!\n";
    ros::spin();
}
