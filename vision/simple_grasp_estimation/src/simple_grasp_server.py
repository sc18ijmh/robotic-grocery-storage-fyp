#!/usr/bin/python
import rospy
import math
import numpy as np
import copy
from copy import deepcopy
from spherical_grasps_server import SphericalGrasps
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from moveit_msgs.msg import Grasp, GripperTranslation
from geometry_msgs.msg import Point, Pose, Quaternion, PoseStamped
from tf import transformations
from tf.transformations import quaternion_from_euler, euler_from_quaternion, quaternion_about_axis, unit_vector, quaternion_multiply
from simple_grasp_estimation.srv import GenerateGrasps, GenerateGraspsResponse, GeneratePlaces, GeneratePlacesResponse
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
from dynamic_reconfigure.server import Server
from simple_grasp_estimation.cfg import SphericalGraspConfig

marker_pub = rospy.Publisher('/simple_grasps', Marker, queue_size = 100)

def visualise_pose(pose, id):
    """
        Publishes an arrow marker to marker_pub, visualiseing a given pose.

        Arguments:
            geometry_msgs/Pose: pose to visualise
            int: numerical id
    """
    vis_pose = Marker()
    vis_pose.header.frame_id = "base_footprint"
    vis_pose.header.stamp = rospy.Time.now()
    vis_pose.id = id
    vis_pose.ns = "simple_grasps"
    vis_pose.type = Marker.ARROW
    vis_pose.action = Marker.ADD
    vis_pose.pose.position = pose.position
    vis_pose.pose.orientation = pose.orientation
    scale = 0.05
    vis_pose.scale.x = scale
    vis_pose.scale.y = scale
    vis_pose.scale.z = scale
    vis_pose.color.g = 1.0
    vis_pose.color.a = 1.0
    vis_pose.lifetime = rospy.Duration()
    marker_pub.publish(vis_pose)


class FaceProperties():
    def __init__(self, vector, quaternion, width):
        self.vector = vector
        self.quat = quaternion
        self.side_width = width


class GraspGenerator():
    def __init__(self):
        self.sg = SphericalGrasps()
        self.gg_serv = rospy.Service('simple_grasps', GenerateGrasps, self.generate_grasp_poses)
        self.pg_serv = rospy.Service('simple_placings', GeneratePlaces, self.generate_placings)
        rospy.loginfo("Simple grasp generator initialised.")


    def generate_placings(self, request):
        """
			Generates placings using spherical_grasps_server.

            Arguments:
                geometry_msgs/Pose: pose at which to generate placings

			Returns:
				PlaceLocations[]: list of placings to attempt at a given point in 3D space
		"""
        placings = []
        placings.extend(self.sg.create_placings_from_object_pose(request.pose))
        placings_res = GeneratePlacesResponse()
        placings_res.placings = placings
        return placings_res


    def generate_grasp_poses(self, request):
        """
            Generates a set of moveit_msgs/Grasp messages for a given pose.
            If the x and y dimensions of the object to be grasped are smaller than
            the gripper aperture generate spherical grasps. Otherwise sample simple 
            grasps on those faces where the width < gripper aperture.

            Arguments:
                geometry_msgs/Pose: pose at which to generate grasps
                geometry_msgs/Point: dimensions of object to be grasped

            Returns:
                moveit_msgs/Grasp[]: list of grasps to attempt for the given pose
        """
        grasps = []
        #if some face of the object is larger than the gripper aperture
        if(request.size.x > (0.038 * 2) + 0.02 
            or request.size.y > (0.038 * 2) + 0.02):
            rospy.loginfo("Asymmetrical shape, generating non-spherical grasps.")
            grasps.extend(self.asymmetrical_grasps(request.pose, request.size))
        else:
            rospy.loginfo("Generating spherical grasps.")
            grasps.extend(self.sg.create_grasps_from_object_pose(request.pose))
        
        grasp_res = GenerateGraspsResponse()
        grasp_res.grasps = grasps
        return grasp_res


    def asymmetrical_grasps(self, pose, size):
        """
            Generate grasps at each face where width < gripper_aperture

            Arguments:
                geometry_msgs/Pose: pose at which to generate grasps
                geometry_msgs/Point: dimensions of object to be grasped

            Returns:
                moveit_msgs/Grasp[]: list of grasps to attempt for the given pose
        """

        #identify and exclude sides with size greater than aperture of TIAGo gripper
        position = pose.pose.position
        asym_poses = []

        #front face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(0.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([-1, 0, 0], Quaternion(*quat), size.y), size, position))
        
        #back face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(180.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([1, 0, 0], Quaternion(*quat), size.y), size, position))

        #top face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(90.0), math.radians(180.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([0, 0, 1], Quaternion(*quat), size.y), size, position))

        #right face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(90.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([0, -1, 0], Quaternion(*quat), size.x), size, position))

        #left face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(0.0), math.radians(270.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([0, 1, 0], Quaternion(*quat), size.x), size, position))

        #top face
        quat = quaternion_from_euler(math.radians(0.0), math.radians(90.0), math.radians(270.0))
        asym_poses.extend(
            self.gen_face_poses(FaceProperties([0, 0, 1], Quaternion(*quat), size.x), size, position))

        if len(asym_poses) == 0:
            return []

        grasps = []

        for idx, pose in enumerate(asym_poses):
            visualise_pose(pose, idx)
            grasps.append(self.sg.create_grasp(pose, "grasp_" + str(idx)))
        
        return grasps

    def gen_face_poses(self, fp, size, position):
        """
            Generate grasps at each face where width < gripper_aperture

            Arguments:
                FaceProperties: contains vector and orientation for this face
                geometry_msgs/Point: dimensions of object to be grasped
                geometry_msgs/Point: center of the object 

            Returns:
                geometry_msgs/Pose[]: list of poses to generate grasps at
        """

        vector = fp.vector
        poses = []
        pose_point = Point()
        size_arr = [size.x, size.y, size.z]

        axis_idx = 0
        #check the face is smaller than gripper aperture
        for idx in range(0, 2):
            if vector[idx] != 0:
                if idx == 2:
                    axis_idx = 0
                    break
                if fp.side_width > (0.038 * 2) + 0.02:
                    return []

        #translate pose_point in front of face indicated by vector
        pose_point.x = (vector[0] * size_arr[0]/2) + (0.1 * vector[0]) #extend point past face surface
        pose_point.y = (vector[1] * size_arr[1]/2) + (0.1 * vector[1])
        pose_point.z = (vector[2] * size_arr[2]/2) + (0.1 * vector[2])

        #translate pose to object frame position
        pose_point.x += position.x
        pose_point.y += position.y
        pose_point.z += position.z

        poses.append(Pose(Point(pose_point.x, pose_point.y, pose_point.z), fp.quat))
        return poses

if __name__ == "__main__":
    rospy.init_node("simple_grasp_estimation")
    gg = GraspGenerator()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

